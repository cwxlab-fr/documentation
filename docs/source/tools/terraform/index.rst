Terraform
=========

why Terraform ?
---------------

Ansible, Chef, Puppet, etc ...
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

https://www.terraform.io/intro/vs/chef-puppet.html

Ces outils de gestion de la configuration installe et configure des logiciels sur des machines déjà existante, ce que Terraform ne fait pas.

Terraform va permettre de déployer les ressources, alors que ces outils vont servir a bootstrapper (amorcer) et initialiser (configuration initial) ces ressources.

Terraform se concentre sur un niveau d'abstraction élevé alors que ces outils vont permettre de configurer précisément les ressources (fichier, permissions, ...)


Il est possible d'utiliser Terraform pour déclencher un bootstrap avec cloud-init.

Cela n'est possible que si le provider le permet, et Terraform se contente de passer un fichier de configuration et n'execute pas lui même les actions sur les systèmes.

Il s'agit la du maximum que Terraform va pouvoir agir sur un système.

cloud-init.yaml:

.. code-block:: yaml

   #cloud-config
   packages:
     - gcc
     - python3-devel
   package_update: true
   package_upgrade: true
   package_reboot_if_required: true
   runcmd:
   # DATA disk setup
     - pvcreate /dev/sdc
     - vgcreate -s 4M vgdata /dev/sdc
     - lvcreate -l 10239 -n lvdata vgdata
     - mkfs.xfs /dev/vgdata/lvdata
     - mkdir /data
   # CRON script setup
     - alternatives --set python /usr/bin/python2
     - pip3 install azure-cli
     - pip3 install azure-storage-blob
     - pip3 install pika
   mounts:
     - [ "/dev/vgdata/lvdata", "/data", "xfs", "defaults", "0", "0" ]
   final_message: Cloud Init is done


ARM, CloudFormation, Heat, etc ...
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- CloudFormation: AWS
- Heat: OpenStack
- ARM: Azure

Ces outils permettent de codifier les details d'une infrastructure dans un ficher de configuration, Terraform est inspiré de cela.

Terraform va plus loin dans la configuration d'infrastructure en étant pas lié a un cloud en particulier (cloud agnostic) et en permettant la combinaison de diffèrent provider dans une même configuration.
Cela va par exemple permettre de faire du multi cloud (Déployer un service sur Azure et AWS dans une même configuration).

De plus, Terraform apporte des concepts comme la séparation des phases de planning des phases d'exécution, le ressource graph, ...

Boto, Fog, etc ...
^^^^^^^^^^^^^^^^^^

https://www.terraform.io/intro/vs/boto.html

Ces librairies sont utilisé pour fournir un accès natif aux providers cloud en utilisant leurs API.

Certaines sont spécifique a un provider, d'autre offre un relatif niveau d'abstraction.

De manière général elles fournissent un accès au API très bas niveau la ou Terraform fournis un accès haut niveau.

Concepts
--------

Infrastructure As Code (IaC)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'Infrastructure As Code (IaC) consiste a se baser sur des fichiers de configuration plutôt que sur une GUI ou sur des commandes pour gérer son infrastructure.

Ces fichiers de configuration sont rédigé en utilisant un langage de configuration qui décrit l'infrastructure


Change automation
^^^^^^^^^^^^^^^^^^

En combinant les possibilités de l'IaC, du Resource Graph et de l'Execution Plan il est possible d'utiliser Terraform pour fortement automatiser les changements d'infrastructure avec un minimum d'intervention humaine.

.. rubric:: .gitlab-ci.yaml

.. code-block:: yaml
   :linenos:
   :emphasize-lines: 24,29,34,35,48

   image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest

   variables:
     TF_ROOT: ${CI_PROJECT_DIR}/prod
     TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/project-prod
   
   cache:
     key: project-prod
     paths:
       - ${TF_ROOT}/.terraform
   
   before_script:
     - cd ${TF_ROOT}
   
   stages:
     - prepare
     - validate
     - build
     - deploy
   
   init:
     stage: prepare
     script:
       - gitlab-terraform init
   
   validate:
     stage: validate
     script:
       - gitlab-terraform validate
   
   plan:
     stage: build
     script:
       - gitlab-terraform plan
       - gitlab-terraform plan-json
     artifacts:
       name: plan
       paths:
         - ${TF_ROOT}/plan.cache
       reports:
         terraform: ${TF_ROOT}/plan.json
   
   apply:
     stage: deploy
     environment:
       name: production
     script:
       - gitlab-terraform apply
     dependencies:
       - plan
     when: manual
     only:
       - main


Providers
^^^^^^^^^

https://registry.terraform.io/browse/providers

Tout comme Ansible sans modules ne sait pas apporter de modification sur un système (installer un paquet, créer un fichier, ...), Terraform sans providers ne connait pas les API Azure, AWS, GCP, OVH, Scaleway, Proxmox, ...

Lors de l'initialisation d'un projet Terraform les providers nécessaires sont automatiquement installé (Voir terraform init), ce sont eux qui "connaissent" les API des plateforme et permettent de construire les `Resource Graph` et les `Execution Plans`.

Les providers permettent également de faire certain pre-check de la configuration même si cela reste limité car seul l'API de la plateforme peut valider pleinement une configuration.

Resource Graph
^^^^^^^^^^^^^^

https://www.terraform.io/docs/cli/commands/graph.html


L'utilisation d'un `resource graph` rend possible la parallelisation de création, modification et suppression de ressources n'ayant pas de dépendance entre elles, apportant un gain de temps d'exécution.

Pour les ressources ayant des dépendances entre elles, l'ordre de création, modification, suppression sera optimisé afin d'éviter les incohérences.

Cela permet d'effectuer des changements d'infrastructure complexe avec peu d'intervention humaine.

.. code-block:: console

   $ terraform graph

State file
^^^^^^^^^^

Le state file de Terraform contient normalement une description exhaustive de l'infrastructure.

Ce fichier est construit par Terraform et va permettre de savoir, par comparaison avec la configuration, quelles sont les changements a appliquer a l'infrastructure.

Il y a plusieurs raison d'utiliser un state file:

- Manipulation de donnée non disponible en lecteur par l'API du provider (par exemple un mot de passe ou simplement une fonction non disponible).

  Pour savoir quand la donnée a été modifié et dois être réapliqué (plutot que de la réappliquer systématiquement), Terraform va comparer la valeur présente dans la configuration avec celle présebte dans le state file.
- Ajout, Modification, Suppression de ressource

  Lorsqu'une ressource est déclaré dans la configuration il est nécessaire de savoir si elle existe ou non, et si elle existe comment elle est configuré.


Comparaison du cycle de vie avec Ansible
----------------------------------------


Dans Ansible, supprimer une resource de la configuration ne suffit pas a declencer sa suppression. Il est d'abord necessaire de chaner l'état de la ressource en `absent`.

Un fois l'état `absent` appliquer sur toutes l'infrastructure il est possible de supprimer la resource de la configuration.



Cycle de vie Terraform grace au `state file`:

+---------------+---------------+----------------+---------------+
| Configuration | State         | Infrastructure | Action        |
+===============+===============+================+===============+
| Absent        | Absent        | Absent         | Aucune        |
+---------------+---------------+----------------+---------------+
| Présent       | Présent       | Présent        | Aucune        |
+---------------+---------------+----------------+---------------+
| Présent       | Absent        | Absent         | Création      |
+---------------+---------------+----------------+---------------+
| Absent        | Présent       | Présent        | Suppression   |
+---------------+---------------+----------------+---------------+
| Absent        | Absent        | Présent        | Aucune        |
|               |               |                | (incoherence) |
+---------------+---------------+----------------+---------------+
| Présent       | Présent       | Absent         | Aucune        |
|               |               |                | (incoherence) |
+---------------+---------------+----------------+---------------+
| Absent        | Présent       | Absent         | Suppression   |
|               |               |                | (incoherence) |
+---------------+---------------+----------------+---------------+
| Présent       | Absent        | Présent        | Création      |
|               |               |                | (incoherence) |
+---------------+---------------+----------------+---------------+


**Gestion du state file**


Le `state file` pouvant contenir des informations sensible (Mot de passe, IP, ...) il faut donc faire particulièrement attention à sont stockage.

Terraform propose deux méthodes pour gérer ce fichier:

.. rubric:: Local

Le `state file` est stocké sur un système de fichier accessible, soit en local soit sur un NAS par exemple.

Cette méthode fonctionne bien pour du test car elle ne demande aucune configuration, mais n'est pas adapté pour la sécurisation du fichier et pour le travail en équipe.

Deux personnes utilisant le même state file en même temps vont créer une divergence en l'état réel de l'infrastructure et le state file.

Si le fichier est perdu il faudra alors le recréer, en reimportant l'infrastructure par exemple.


.. rubric:: Backend HTTP

Le fichier est sur un backend HTTP (type stockage objet: OpenStack Swift, Amazon S3, ...).

L'accès se fait de manière sécurisé avec une gestion des droits et une connexion chiffré, un système de lock permet d'eviter que deux exécutions de Terraform ne puissent pas utiliser le fichier en même temps.

Cette méthode est recommandé en production et plus généralement dés que du travail en équipe est prévu.


**GitLab Terraform HTTP backend**


GitLab propose, dans sa version gratuite un backend HTTP pour les state file Terraform: https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html


Project size considerations
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Quand un projet va prendre de l'importance il va devenir contraignant de se limiter aux fichiers conventionnel, on va donc decouper la configuration en plus de fichiers.

Il n'y a pas de convention pour ce decoupage cela va devoir se faire au cas par cas.

Il n'y a pas de limites technique a la taille d'un projet Terraform, ce qui posera problème en premier sera la taille ou le nombre des fichiers de configuration, la quantité de changements, le nombre d'output, etc... rendront la maintenance trés difficile. On perd l'interet d'une validation humaine simplifié


Usage
-----

Terraform s'execute de la façon suivante:

  .. code-block:: console

   $ terraform [global options] <subcommand> [args]

.. program:: terraform


.. option:: init

   Initialise un repertoire de travail contenant des fichiers de configuration Terraform. C'est la premiére commande qui devrait être exécuté aprés avoir écrit une nouvelle configuration Terraform ou aprés en avoir cloné une (depuis un SCM). 

   Executer cette commande plusieurs fois ne présente pas de risque (Aucun action sur l'infrastructure).

.. option:: validate

   Effectue une validation des fichiers de configuration dans un répertoire, se limite exclusivement à la configuration et n'accede a aucun service distant tel que les `remote state`, `provider API`, etc ...

.. option:: plan

   Crée un `execution plan`. Par default la creation d'un plan consiste a:

   * Lis l'état de chaque objet distant déjà existant afin de s'assurer que le `state` est à jour.
   * Compare la configuration actuel avec le `state` et note les differences.
   * Propose un ensemble d'action modificatrice qui, si elles sont appliqués, feront correspondre les objets distant avec la configuration.

.. option:: apply

   Applique les actions proposé par le `plan`.

.. option:: destroy

   Devrait être utilisé comme une sous commande de `apply`:

   .. code-block:: console

     $ terraform apply -destroy

   Detruis tout les objets distant géré par une configuration Terraform donnés
 
.. option:: import

   trouve la resource grace a sont ID et l'importe dans le `state`.

   Syntaxe:

   .. code-block:: console

    $ terraform \
          import
          azurerm_app_service_plan.instance1 \
          /subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/mygroup1/providers/Microsoft.Web/serverfarms/instance1

