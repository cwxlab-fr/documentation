Windows Subsystem for Linux (WSL)
=================================

.. seealso::

   https://github.com/microsoft/WSL/issues/5611

DNS (resolv.conf)
-----------------

.. code-block:: ini
  :caption: /etc/wsl.conf

   [network]
   generateResolvConf = false

.. code-block:: powershell

   PS C:\Windows\system32> wsl --shutdown

Le fichier :file:`/etc/resolv.conf` est un lien symbolique qui pointe sur :file:`../run/resolvconf/resolv.conf` (:file:`/run/resolvconf/resolv.conf`).
Il faut supprimer le lien avant de recréer le fichier.

.. code-block:: console

   $ sudo rm /etc/resolv.conf
   $ sudo touch /etc/resolv.conf

On utilise les DNS de FDN [#fdn-dns]_ ici.

.. code-block:: text

   nameserver 80.67.169.12
   nameserver 80.67.169.40


.. rubric:: footnotes

.. [#fdn-dns] https://www.fdn.fr/actions/dns/

