Helm
====

.. seealso::
    
    https://helm.sh/docs/intro/install/


Installation
------------

Debian
~~~~~~

Recupérons la clef GPG.
Il n'est pas necessaire de la `dearmor` depuis Debian `10/buster`

.. code-block:: console

    $ sudo mkdir -p /etc/apt/keyrings
    $ sudo curl -L https://baltocdn.com/helm/signing.asc -o /etc/apt/keyrings/helm.asc
    $ echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/helm.asc] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    $ sudo apt-get update
    $ sudo apt-get install helm


Create a chart
--------------

.. seealso::

   * https://gitlab.com/cwxlab-fr/library/helm/killing-floor-2-dedicated-server

.. code-block:: console

   $ helm create NAME


.helmignore
~~~~~~~~~~~

:file:`.helmignore`

.. code-block::

   # Patterns to ignore when building packages.
   # This supports shell glob matching, relative path matching, and
   # negation (prefixed with !). Only one pattern per line.
   .DS_Store
   # Common VCS dirs
   .git/
   .gitignore
   .bzr/
   .bzrignore
   .hg/
   .hgignore
   .svn/
   # Common backup files
   *.swp
   *.bak
   *.tmp
   *.orig
   *~
   # Various IDEs
   .project
   .idea/
   *.tmproj
   .vscode/

Chart.yaml
~~~~~~~~~~

:file:`Chart.yaml`

.. code-block:: yaml

   apiVersion: v2
   name: <name>
   description: <description>
   icon: https://<my-icon-address>

   type: application

   version: 0.1.0

   appVersion: "1.0.0"

Values.yaml
~~~~~~~~~~~

:file:`Values.yaml`

.. code-block:: yaml

   #nameOverride:
   image:
     repository: registry.gitlab.com/cwxlab-fr/library/container-image/<image>
     tag: 0.1.0

templates/_helpers.tpl
~~~~~~~~~~~~~~~~~~~~~~

:file:`templates/_helpers.tpl`

`Name` is truncated at 63 chars because some Kubernetes name fields are limited
to this (by the DNS naming spec).

.. code-block:: yaml

   {{- define "default.name" -}}
   {{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
   {{- end }}

   {{- define "default.chart" -}}
   {{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
   {{- end }}

   {{- define "default.selectorLabels" -}}
   app.kubernetes.io/name: {{ include "default.name" . }}
   app.kubernetes.io/instance: {{ .Release.Name }}
   {{- end }}
   
   {{- define "default.labels" -}}
   helm.sh: {{ include "default.chart" . }}
   {{ include "default.selectorLabels" .}}
   app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
   app.kubernetes.io/managed-by: {{ .Release.Service }}
   {{- end }}


templates/deployment.yaml
~~~~~~~~~~~~~~~~~~~~~~~~~

:file:`templates/deployment.yaml`

.. code-block:: yaml

   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: {{ .Release.Name }}
     labels:
       {{- include "default.labels" . | nindent 4 }}
   spec:
     replicas: 1
     selector:
       matchLabels:
         app.kubernetes.io/name: {{ .Chart.Name }}
         app.kubernetes.io/instance: {{ .Release.Name }}
     template:
       metadata:
         labels:
           app.kubernetes.io/name: {{ .Chart.Name }}
           app.kubernetes.io/instance: {{ .Release.Name }}
       spec:
         containers:
           - name: {{ .Chart.Name }}
             image: {{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}
             imagePullPolicy: IfNotPresent

Publication
-----------

GitLab CI
~~~~~~~~~

.. code-block:: console

   curl -v \
       --request POST \
       --user gitlab-ci-token:${CI_JOB_TOKEN} \
       --form "chart=@build/killing-floor-2-dedicated-server-${CI_COMMIT_TAG}.tgz" \
       "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"


Options helm
------------

.. program:: helm

.. option:: create

   This command creates a chart directory along with the common files and
   directories used in a chart.

   Use: helm create myChart

.. option:: lint

   This command takes a path to a chart and runs a series of tests to verify that
   the chart is well-formed.

   Use: helm lint --strict --with-subcharts .

.. option:: package

   This command packages a chart into a versioned chart archive file.

   Use: mkdir build && cd build && helm package .. --version 0.1.0
