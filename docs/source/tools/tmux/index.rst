tmux
====

tmux.conf
---------

.. seealso::

   https://github.com/tmux/tmux
   https://unix.stackexchange.com/questions/210174/set-the-active-tmux-tab-color

Configuration for tmux >= 3
Tested on `tmux 3.3a`

:file:`~/.tmux.conf`

.. code-block:: text

   # Enable mouse support
   set-option -g mouse on
   
   # Start indexing window at 1 (0 is not convenient on keyboards...)
   set-option -g base-index 1
   
   # Set the maximum number of lines held in window history.
   set-option -g history-limit 60000

   # Use vi-style key bindings in copy mode.
   set-option -g mode-keys vi

   # Enable 24-bit truecolor
   # * https://dandavison.github.io/delta/tips-and-tricks/using-delta-with-tmux.html
   # * https://github.com/tmux/tmux/issues/696
   set-option -g terminal-overrides ",xterm-256color:Tc"

   # Highlight current tab in yellow
   set-window-option -g window-status-current-style bg=yellow

   # Reload configuration with `prefix+r`
   bind-key -T prefix r source-file ~/.tmux.conf \; display "Reloaded: ~/.tmux.conf"

   # Enter copy-mode with `prefix+y`
   bind-key -T prefix y copy-mode


   ### Key binding when in `copy-mode`
   # Replace `copy-pipe-and-cancel` by `copy-pipe` when selecting by dragging
   # the mouse
   bind-key -T copy-mode MouseDragEnd1Pane send-keys -X copy-pipe
   bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe

   # Start selecting with `v`
   bind-key -T copy-mode-vi v send-keys -X begin-selection

   # Start selection a rectangle with `Ctrl+v`
   bind-key -T copy-mode-vi C-v send-keys -X begin-selection \; send-keys -X rectangle-toggle

   # copy current selection and send it to xclip with `y`
   bind-key -T copy-mode-vi y send-keys -X copy-pipe "xclip -i -f -selection primary | xclip -i -selection clipboard"


   ### Window manipulation
   # Create a new window with `prefix+c`
   bind-key -T prefix c new-window -c "#{pane_current_path}"

   # Split the current window horizontaly with `prefix+h`
   bind-key -T prefix v split-window -h -c "#{pane_current_path}"

   # Split the current window verticaly with `prefix+h`
   bind-key -T prefix h split-window -v -c "#{pane_current_path}"
   

   ### Window navigation
   # Go to the next window with `Alt+Right`
   bind-key -T root M-Right next-window

   # Go to the previous window with `Alt+Left`
   bind-key -T root M-Left previous-window
   

   ### Pane navigation
   # Select the down pane with `Shift+Down`
   bind-key -T root S-Down select-pane -D

   # Select the left pane with `Shift+Left`
   bind-key -T root S-Left select-pane -L
   
   # Select the right pane with `Shift+Right`
   bind-key -T root S-Right select-pane -R

   # Select the up pane with `Shift+Up`
   bind-key -T root S-Up select-pane -U
