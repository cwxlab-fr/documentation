Kubectl
=======

.. seealso::

    https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/


Installation
------------

Debian
^^^^^^

Sur debian kubectl est disponible directement dans les repos

.. seealso::

    * https://packages.debian.org/bullseye/kubernetes-client
    * https://packages.debian.org/bookworm/kubernetes-client

.. code-block:: console

    $ https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/