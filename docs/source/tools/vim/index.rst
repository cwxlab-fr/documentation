Vim
===

.. seealso::

   https://vimdoc.sourceforge.net/htmldoc/options.html

NERDTree
--------

.. seealso::

   https://github.com/preservim/nerdtree

Installation
^^^^^^^^^^^^

.. code-block:: console

   $ git clone https://github.com/preservim/nerdtree.git ~/.vim/pack/vendor/start/nerdtree
   $ vim -u NONE -c "helptags ~/.vim/pack/vendor/start/nerdtree/doc" -c q

Flake8
------

.. seealso::

   https://github.com/nvie/vim-flake8

Installation
^^^^^^^^^^^^

.. code-block:: console

   $ sudo apt-get install --yes flake8

   $ mkdir -p ~/.vim/pack/flake8/start/
   $ git clone https://github.com/nvie/vim-flake8.git ~/.vim/pack/flake8/start/vim-flake8

vimrc
-----

:file:`~/.vimrc`

.. code-block:: text

   """ Vim directories
   " Create silently backup directory
   " `~/.vim/backups` will prevent swap and backup file to be either in the
   " current directory (bad for version control system) or in /tmp (bad for
   " security on shared system)
   silent !mkdir -p ~/.vim/backups

   " Set the backup directory to `~/.vim/backups`
   set backupdir=~/.vim/backups

   " Set the swap file directory to `~/.vim/backups`
   set directory=~/.vim/backups

   " Set the undo directory to `~/.vim/backups`
   set undodir=~/.vim/backups


   """ Miscelaneous
   " Enable undo history file
   set undofile

   " Set the character encoding to utf-8.
   set encoding=utf-8


   """ Tab management
   " Insert spaces instead of <Tab>.
   set expandtab
   " Insert 4 spaces to make a <Tab>.
   set tabstop=4


   """ Files informations
   " Always display the status line.
   set laststatus=2

   " Show the line and column number of the cursor position.
   set ruler

   " Enable syntax highlighting
   set syntax=on

   " Highligh column 80 and 100
   set colorcolumn=80,100
   

   """ Map file extentions with filetype
   " *.sls is the extention for SaltStack state files (yaml)
   autocmd BufRead,BufNewFile *.sls setfiletype yaml


   """ NERDTree
   " Start NERDTree and put the cursor back in the other window.
   autocmd VimEnter * NERDTree | wincmd p
   
   " Exit Vim if NERDTree is the only window remaining in the only tab.
   autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

   " Close the tab if NERDTree is the only window remaining in it.
   " Vim < 9.0.907
   "autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
   " Vim >= 9.0.907
   " Thanks to Gray Johnson (https://groups.google.com/g/vim_dev/c/Cw8McBH6DDM/m/-O7UhK_OAgAJ)
   autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif

   " Open the existing NERDTree on each new tab.
   autocmd BufWinEnter * if getcmdwintype() == '' && &buftype != 'quickfix' | silent NERDTreeMirror | endif

   " Hide some stuff from the NERDTree tree
   let NERDTreeIgnore=['^__pycache__$[[dir]]', '\.egg-info$[[dir]]']


   """ Flake8
   " Run Flake8 check every time a Python file is written
   autocmd BufWritePost *.py call flake8#Flake8()

   " Exit Vim if only a quickfix window remain
   autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && &buftype == 'quickfix' | quit | endif
