Sphinx Doc
==========

.. seealso::

  * https://www.sphinx-doc.org/en/master/
  * https://sphinx-themes.org/
  * https://readthedocs.org/

Cette documentation parle de l'automatisation de la construction de configuration
Sphinx Doc avec GitLab CI/CD.

Initialisation
--------------

Pour des raisons de simplicité l'initialisation se fera en local (clone du projet,
initialisation, push).

Les logiciels necessaire pour cette initialisation sont :

* Git
* Docker
* N'importe quel editeur de texte

Clonons le dépôt localement.

.. code-block:: console

   $ git clone git@gitlab.com:cwxlab-fr/documentation.git
   $ cd documentation

Démarrage rapide (QuickStart) du projet Sphinx Doc.  
Il sera necessaire de changer la valeur de `--project CwxLab.fr` et 
`--author Crawax`.

.. code-block:: console

   $ docker run \
         --rm \
         -it \
         --user "${UID}:${GID}" \
         -v "$(pwd):/docs" \
         sphinxdoc/sphinx sphinx-quickstart \
             --sep \
             --project CwxLab.fr \
             --author Crawax \
             -v 0.1 \
             --release 0.1 \
             --language fr

Options docker-run
^^^^^^^^^^^^^^^^^^

.. seealso::

   https://docs.docker.com/engine/reference/commandline/run/

.. program:: docker-run

.. option:: --rm

   Automatically remove the container when it exits

.. option:: --interactive , -i

   Keep STDIN open even if not attached

.. option:: --tty , -t

   Allocate a pseudo-TTY

.. option:: --user , -u

   Username or UID (format: <name|uid>[:<group|gid>])

.. option:: --volume , -v

   Bind mount a volume


Options sphinx-quickstart
^^^^^^^^^^^^^^^^^^^^^^^^^

.. seealso::

   https://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html

.. program:: sphinx-quickstart

.. option:: --sep

   If specified, separate source and build directories.

.. option:: -p PROJECT, --project=PROJECT

   Project name will be set.

.. option:: -a AUTHOR, --author=AUTHOR

   Author names.

.. option:: -v VERSION

   Version of project.

.. option:: -r RELEASE, --release=RELEASE

   Release of project.

.. option:: -l LANGUAGE, --language=LANGUAGE

   Document language.

GitLab
------

.. code-block:: yaml

   image: sphinxdoc/sphinx
   
   workflow:
     rules:
       - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
       - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
         when: never
       - if: '$CI_COMMIT_BRANCH'
   
   before_script:
       - python3 -m pip install --no-cache-dir sphinx-rtd-theme
       - cd docs && make html && cd ..
   
   pages:
     stage: deploy
     script:
       - mv docs/build/html public
     artifacts:
       paths:
         - public
     rules:
       - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
         changes:
           - docs/source/**/*
   
   test:
     stage: test
     script:
       - mv docs/build/html test
     artifacts:
       paths:
         - test
     rules:
       - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
         changes:
           - docs/source/**/*



image
^^^^^

.. code-block:: yaml

   image: sphinxdoc/sphinx

Nous utilisons l'image Docker officiel Sphinx Doc.
Il existe une image alternative `sphinxdoc/sphinx-latexpdf` mais celle ci est 
netement plus volumineuse et ne sera utile que pour faire du LaTex et/ou du PDF.

.. seealso::

   * https://docs.gitlab.com/ee/ci/yaml/index.html#image
   * https://hub.docker.com/r/sphinxdoc/sphinx
   * https://hub.docker.com/r/sphinxdoc/sphinx-latexpdf

workflow
^^^^^^^^

.. code-block:: yaml

   workflow:
     rules:
       - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
       - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
         when: never
       - if: '$CI_COMMIT_BRANCH'

Afin de definir des régles d'execution global de la pipeline on utilise la 
directive `workflow`.
Cela permet de ne pas avoir à définir les régles global sur tout les jobs.

Ici les régles définissent que la pipeline se lance si :

* La source de declencement est une merge request
* Si la pipeline s'éxécute sur une branche ET qu'il n'y à PAS une merge request
  ouvert correspondant. Cela pour eviter de lancer une pipeline au commit puis 
  a la merge request. Ce n'est pas obligatoire.

.. seealso::

   * https://docs.gitlab.com/ee/ci/yaml/index.html#workflow
   * https://docs.gitlab.com/ee/ci/yaml/index.html#rules

before_scripts
^^^^^^^^^^^^^^

.. code-block:: yaml

   before_script:
    - python3 -m pip install --no-cache-dir sphinx-rtd-theme
    - cd docs && make html && cd ..

`before_scripts` permet d'executer des commandes avant chaque jobs sans avoir 
besoin de les repreciser a chaque fois.

Ici les commandes permettent de

* Installer les dependances non présente dans l'images Docker. Ici le théme.
* Construire la documentation

.. seealso::

   https://docs.gitlab.com/ee/ci/yaml/index.html#before_script
