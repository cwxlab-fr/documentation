Git SCM
=======

.. seealso::

   https://git-scm.com/

Installation
------------

Debian
~~~~~~

.. code-block:: console

   $ sudo apt-get install git

Configuration
-------------

.. seealso::

   * https://git-scm.com/book/fr/v2/Personnalisation-de-Git-Configuration-de-Git
   * `man git-config`

On utilisera le plus souvent le format de commande `git config --global [CONFIG]`.
Cela permet d'écrire la configuration dans le fichier :file:`~/.gitconfig`, cela
permet d'avoir une configuration qui sera appliqué sur tout les dépôts.

Ne pas préciser `--global` fera que la confiration sera spécifique au dépôt actif.


User name and email
~~~~~~~~~~~~~~~~~~~

La seule configuration obligatoire est celle du nom d'utilisateur et du mot de 
l'adresse mail.
Sans cette configuration initial Git refusera de push ou definira automatiquement
ces paramètres, ce qui pourrait avoir des effets indesirables.

.. code-block:: console

   $ git config --global user.name "John Doe"
   $ git config --global user.email "johndoe@example.com"


Pager
~~~~~

.. seealso::

   * https://github.com/dandavison/delta
   * https://dandavison.github.io/delta/installation.html
   * https://github.com/dandavison/delta/releases


.. code-block:: console

   $ curl -JOL https://github.com/dandavison/delta/releases/download/0.16.5/git-delta_0.16.5_amd64.deb
   $ sudo apt-get install ./git-delta_0.16.5_amd64.deb

   $ git config --global core.pager "delta"
   $ git config --global interactive.diffFilter "delta --color-only"
   $ git config --global delta.navigate "true"  # use n and N to move between diff sections
   $ git config --global delta.light "false"  # set to true if you're in a terminal w/ a light background color
   $ git config --global merge.conflictstyle "diff3"
   $ git config --global diff.colorMoved "default"
   $ git config --global delta.line-numbers "true"

Alias
~~~~~

Les alias Git fonctionnent de façons simimaire aux alias des Schell Unix.
Les alias ont deux interets majeurs

* Raccourcir les commandes Git utilisé au quotidien (checkout, commit, ...)
* Permettre l'usage de fonctionnalité avancé, par exemple la mise en page avancé
  avec `git log`.

.. code-block:: console

   $ git config --global alias.co "checkout"
   $ git config --global alias.br "branch"
   $ git config --global alias.ci "commit"
   $ git config --global alias.st "status"
   $ git config --global alias.unstage "reset HEAD --"
   $ git config --global alias.last "log --stat -1 HEAD"
   $ git config --global alias.log8 'log --graph --abbrev-commit --decorate --date=relative --format=format:"%C(bold yellow)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(bold white)%s%C(reset) %C(white)- %an%C(reset)%C(bold yellow)%d%C(reset)" --all'
   $ git config --global alias.log10 'log --graph --abbrev-commit --decorate --date=relative --format=format:"%C(bold yellow)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(bold white)%s%C(reset) %C(white)- %an%C(reset)%C(bold yellow)%d%C(reset)" --all -30'
   $ git config --global alias.log50 'log --graph --abbrev-commit --decorate --date=relative --format=format:"%C(bold yellow)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(bold white)%s%C(reset) %C(white)- %an%C(reset)%C(bold yellow)%d%C(reset)" --all -30'

Editeur
~~~~~~~

.. code-block:: console

   git config --global core.editor vim

Clef SSH
~~~~~~~~

.. seealso::

   https://superuser.com/questions/232373/how-to-tell-git-which-private-key-to-use

Pour configurer la clef SSH à utiliser sans modifier sa configuration SSH

.. code-block:: console

   # Pour un nouveau repo
   $ git clone -c "core.sshCommand=ssh -i ~/.ssh/<private-key>" git@github.com:<org>/<repo>.git

   # Pour un repo existant, nous ne mettons pas '--global' dans ce cas car la
   # configuration est specifique eu repo
   $ git config core.sshCommand "ssh -i ~/.ssh/<private-key>"

   # Pour verifier
   $ git config --get core.sshCommand
   ssh -i ~/.ssh/<private-key>
