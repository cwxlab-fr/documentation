SSH Client
==========

Installation
------------

Debian
~~~~~~

.. code-block:: console

   $ sudo apt-get install --yes openssh-client


Configuration
-------------

~/.ssh/config
~~~~~~~~~~~~~

.. code-block::

   # Configuration global pour tout les hosts
   Host *
       IdentityFile ~/.ssh/myuser/id_rsa

   # Configuration pour un host unique
   Host myhost
       IdentityFile ~/.ssh/<my-user>/myhost_id_rsa

   # Configuration pour plusieurs host
   Host *.local myhost
       User myuser

   # Compatibilité avec d'ancien host
   Host oldsrv
       HostKeyAlgorithms=+ssh-rsa
       PubkeyAcceptedKeyType +ssh-dss,ssh-rsa

   # Definis l'utilisateur si aucun n'est spécifié (a la place de l'utilisateur
   # courant)
   Host myhost
       User myuser

   # Definis une clef SSH, a la place de toute les essayers
   Host myhostnopassword
       IdentityFile ~/.ssh/mykey

   # Definis un alias (`ssh easyname` à la place de `ssh 192.168.1.100`)
   Host easyname
       Hostname 192.168.1.100

   # Active le forward X11
   Host x11host
        ForwardX11 yes

   # Connection a un host a travers un proxy SSH
   Host nodirectlink
       ProxyCommand ssh -W %h:%p proxyhost
