Killbenetes Floor 2
===================

.. seealso::
    * https://www.killingfloor2.com
    * https://wiki.killingfloor2.com/index.php?title=Main_Page
    * https://www.scaleway.com

Projet pour deployer un serveur `Killing Floor 2` dans un cluster `Kubernetes` chez `Scaleway`.

Les repos:

* https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2
  * Contient la partie terraform
* https://gitlab.com/cwxlab-fr/library/helm/killing-floor-2-dedicated-server
  * Contient le registre Helm du serveur `Killing Floor 2`