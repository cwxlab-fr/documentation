Killing Floor 2 - Serveur dédié
===============================

Killing Floor 2 [#kf2]_ est un jeu vidéo de Tripwire Interactive [#tripwire]_.

Cette documentation presente le déploiement automatisé d'un serveur dédié Killing floor 2 en utilisant :

* GitLab CI/CD
* Terraform
* Ansible

Ce qui ne sera pas couvert :

* L'hebergement Proxmox

Le but de ce projet est d'avoir une base "simple et rapide", il sera autonome.

Le code est disponible ici: https://gitlab.com/cwxlab-fr/misc/kf2server

TODO
----

* Revoir la pipeline, peut être faire des jobs differents pour la pipeline automatique et la manual

GitLab CI
---------

Presentation
^^^^^^^^^^^^

La pipeline peut être éxécuté de 2 façons différentes

* Automatique: Delenché par une modification des fichiers Terraform et/ou Ansible.
* Manuel: Déclenché par le bouton "Run pipeline" dans l'interface web.

.. figure:: pipeline_auto.png

   Pipeline lancé automatiquement

      Interface GitLab lorsqu'une pipeline est lancé automatiquement

.. figure:: pipeline_manual.png

   Pipeline lancé manuellement

      Interface GitLab lorsqu'une pipeline est lancé manuellement



.gitlab-ci.yml
.gitlab-ci_terraform.yml
.gitlab-ci_ansible.yml

Terraform
---------

variables
^^^^^^^^^

provider
^^^^^^^^

backend
^^^^^^^



Ansible
-------

playbook
^^^^^^^^

inventory
^^^^^^^^^

role
^^^^

.. rubric:: footnotes

.. [#kf2] Killing Floor 2: https://killingfloor2.com
.. [#Tripwire] Tripwire Interactive: https://tripwireinteractive.com
