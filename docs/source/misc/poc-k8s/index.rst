POC Kubernetes
==============

Kubespray
---------

.. seealso::

   https://github.com/kubernetes-sigs/kubespray

.. code-block:: console

   $ git clone --bare https://github.com/kubernetes-sigs/kubespray.git kubespray-tmp
   $ cd kubespray-tmp
   $ git remote set-url origin git@gitlab.com:cwxlab-fr/misc/poc-kubernetes/kubespray.git
   $ git push --mirror
   $ cd ..
   $ rm -rf kubespray-tmp
   $ git clone git@gitlab.com:cwxlab-fr/misc/poc-kubernetes/kubespray.git

