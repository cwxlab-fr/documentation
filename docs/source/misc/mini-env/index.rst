Mini env (GitLab+MediaWiki+ELK+Vault)
=====================================

Petit environement containairisé permettant de faire des tests sur GitLab, 
ElasticSearch, MediaWiki, Hashicorp Vault.

Le but est de présenter l'environement minimal d'un entreprise souhaitant se
lancé dans le DevOps, mais également les outils minimaux d'un Systéme 
d'information moderne.

docker-compose.yml
------------------

.. code-block:: yaml

   version: "3.3"
   
   services:
   
     vault:
       privileged: true
       image: hashicorp/vault
       restart: on-failure
       hostname: 'vault.demo.local'
       command: server
       ports:
         - "8200:8200"
       volumes:
         - ./volumes/vault/file:/vault/file
         - ./volumes/vault/log:/vault/log
         - ./volumes/vault/config:/vault/config
   
     wiki_www:
       image: mediawiki
       restart: always
       hostname: 'wiki.demo.local'
       ports:
         - 8080:80
       volumes:
         - ./volumes/wiki_www/images:/var/www/html/images
         # After initial setup, download LocalSettings.php to the same directory as
         # this yaml and uncomment the following line and use compose to restart
         # the mediawiki service
         - ./volumes/wiki_www/LocalSettings.php:/var/www/html/LocalSettings.php
       depends_on:
         - wiki_db
   
     wiki_db:
       image: mariadb
       restart: always
       hostname: 'wikidb.demo.local'
       volumes:
         - ./volumes/wiki_db/mysql:/var/lib/mysql
       environment:
         MYSQL_DATABASE: "${WIKI_MYSQL_DATABASE}"
         MYSQL_USER: "${WIKI_MYSQL_USER}"
         MYSQL_PASSWORD: "${WIKI_MYSQL_PASSWORD}"
         MYSQL_RANDOM_ROOT_PASSWORD: 'yes'
     es01:
       image: elasticsearch:7.14.2
       hostname: 'es01.demo.local'
       environment:
         - node.name=es01
         - cluster.name=es-docker-cluster
         - discovery.seed_hosts=es02,es03
         - cluster.initial_master_nodes=es01,es02,es03
         - bootstrap.memory_lock=true
         - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
       ulimits:
         nofile:
           soft: 65535
           hard: 65535
         memlock:
           soft: -1
           hard: -1
       volumes:
         - ./volumes/es01/data:/usr/share/elasticsearch/data
       ports:
         - 9200:9200
   
     es02:
       image: elasticsearch:7.14.2
       container_name: elasticsearch_02
       hostname: 'es02.demo.local'
       environment:
         - node.name=es02
         - cluster.name=es-docker-cluster
         - discovery.seed_hosts=es01,es03
         - cluster.initial_master_nodes=es01,es02,es03
         - bootstrap.memory_lock=true
         - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
       ulimits:
         nofile:
           soft: 65535
           hard: 65535
         memlock:
           soft: -1
           hard: -1
       volumes:
         - ./volumes/es02/data:/usr/share/elasticsearch/data
     es03:
       image: elasticsearch:7.14.2
       container_name: elasticsearch_03
       hostname: 'es03.demo.local'
       environment:
         - node.name=es03
         - cluster.name=es-docker-cluster
         - discovery.seed_hosts=es01,es02
         - cluster.initial_master_nodes=es01,es02,es03
         - bootstrap.memory_lock=true
         - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
       ulimits:
         nofile:
           soft: 65535
           hard: 65535
         memlock:
           soft: -1
           hard: -1
       volumes:
         - ./volumes/es03/data:/usr/share/elasticsearch/data
   
     kibana:
       image: kibana:7.14.2
       container_name: kibana
       hostname: 'kibana.demo.local'
       ports:
         - 5601:5601
       environment:
         SERVER_NAME: kibana.demo.local
         ELASTICSEARCH_HOSTS: '["http://es01:9200","http://es02:9200","http://es03:9200"]'
       depends_on:
         - es01
   
     gitlab:
       image: gitlab/gitlab-ce:latest
       restart: always
       hostname: 'gitlab.demo.local'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           external_url 'https://gitlab.demo.local:8929'
           gitlab_rails['gitlab_shell_ssh_port'] = 2224
       ports:
         - '8929:8929'
         - '4443:443'
         - '2224:22'
       volumes:
         - ./volumes/gitlab/config:/etc/gitlab
         - ./volumes/gitlab/logs:/var/log/gitlab
         - ./volumes/gitlab/data:/var/opt/gitlab
   
     gitlab-runner:
       image: gitlab/gitlab-runner:latest
       restart: always
       container_name: gitlab-runner
       hostname: 'gitlab-runner.demo.local'
       volumes:
         - ./volumes/gitlab/config/ssl/gitlab.demo.local.crt:/etc/gitlab-runner/certs/gitlab.demo.local.crt:ro
         - ./volumes/gitlab-runner/config:/etc/gitlab-runner
         - /var/run/docker.sock:/var/run/docker.sock
       depends_on:
         - gitlab
   
File structure
--------------

.. code-block:: raw
  
   └── volumes
       ├── es01
       │   └── data
       ├── es02
       │   └── data
       ├── es03
       │   └── data
       ├── gitlab
       │   ├── config
       │   ├── data
       │   └── logs
       ├── gitlab-runner
       │   └── config
       ├── vault
       │   ├── config
       │   │   └── config.hcl
       │   ├── file
       │   └── log
       ├── wiki_db
       │   └── mysql
       └── wiki_www
           └── images

volumes/vault/config/config.hcl
-------------------------------

.. code-block::

   ui = true
   #disable_mlock = true

   storage "raft" {
     path    = "/vault/file"
     node_id = "node1"
   }

   listener "tcp" {
     address     = "0.0.0.0:8200"
     tls_disable = "true"
   }

   api_addr = "http://127.0.0.1:8200"
   cluster_addr = "https://127.0.0.1:8201"

Bootstrap
---------

.. code-block:: console

   $ docker-compose up vault gitlab wiki_www wiki_db

After GitLab finish initializing `[Ctrl-C]` to stop all.

Run
---

.. code-block:: console

   $ docker-compose up -d vault gitlab wiki_www wiki_db

After the intial bootstrap we can run it inn daemon mode.
