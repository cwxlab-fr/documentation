Killing Floor 2 dedicated server
================================

Cette chart Helm à pour but de deployer un serveur dédié `Killing Floor 2`

Quelques caracteristiques:

* Stockage persitant
* Utilisation d'un `ìnit container` pour mettre à jour steam et le serveur automatiquement
* Definition de service pour le jeu et d'un service utilisant un ingress Nginx pour l'interface web d'administration
* Gestion de la configuration et des identifiants