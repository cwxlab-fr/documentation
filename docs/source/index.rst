.. CwxLab.fr documentation master file, created by
   sphinx-quickstart on Wed Jan 19 18:06:29 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation de CwxLab.fr
===========================================

CwxLab est ma (Crawax) documentation.
Je vais essayer d'avoir une approche DevOps dans mes projets, c'est à dire :

* Automatisation
* Configuration et Infrastructure As Code

Je vais également privilégié les logiciels libre et Open Source. Cela pour des
raison idéologique mais également financiére.

* **Dépôt**: https://gitlab.com/cwxlab-fr/documentation

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Outils

   tools/sphinx-doc/index
   tools/git/index
   tools/terraform/index
   tools/helm/index
   tools/kubectl/index
   tools/tmux/index
   tools/vim/index
   tools/wsl/index
   tools/ssh_client/index

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: IaaS

..   iaas/scaleway/index

.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: PaaS

..   paas/k8s/index

.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: SaaS

..   saas/postgres/index

.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: Bibliothèque

   library/killing-floor-2-dedicated-server/index

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Divers

   misc/mini-env/index
   misc/kf2server/index
   misc/poc-k8s/index
   misc/killbernetes-floor-2/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
