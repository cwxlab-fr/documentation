[[_TOC_]]

# Documentation

## Prerequisite

- Softwares
  - Git
  - Docker

## Initialisation

### Sphinx

**Clone the repository**

```
git clone git@gitlab.com:cwxlab-fr/documentation.git
cd documentation
```

**QuickStart the Sphinx project**

```
docker run \
    --rm \
    -it \
    --user "${UID}:${GID}"
    -v "$(pwd):/docs" \
    sphinxdoc/sphinx sphinx-quickstart \
        --sep \
        --project CwxLab.fr \
        --author Crawax \
        -v 0.1 \
        --release 0.1 \
        --language en
```

**Edit `source/conf.py` to change theme**

```
[...]

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_pdj_theme'

[...]
```

**Create `.gitempty` files to allow git to track empty directory**

```
touch build/.gitempty source/{_static,_templates}/.gitempty
```

### Gitlab CI

```
image: sphinxdoc/sphinx

pages:
  script:
    - python3 -m pip install --no-cache-dir sphinx_pdj_theme
    - make html
    - mv build/html public
  artifacts:
    paths:
      - public
```
